'use strict';
var themeName   = 'bsw',
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    babel       = require('gulp-babel'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    uglify      = require('gulp-uglify-es').default,
    imagemin    = require('gulp-imagemin'),
    rename      = require('gulp-rename'),
    rigger      = require('gulp-rigger'),
    browserSync = require('browser-sync'),
    concat      = require('gulp-concat'),
    buildFolder = 'build/wp-content/themes/' + themeName,
    buildPlugin = 'build/plugins',
    reload      = browserSync.reload;

gulp.task('styles', function () {
    return gulp.src(themeName + '/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
});

gulp.task('scripts', function () {
    return gulp.src(themeName + '/assets/js/*.js')
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(buildFolder + '/assets/js/'));
        
});

gulp.task('default', gulp.series(
    gulp.parallel(
        'styles',
        'scripts'
    )
));

gulp.task('watch', function () {
    gulp.watch(themeName + '/assets/sass/**/*.scss', gulp.series('styles'));

    gulp.watch(themeName + '/assets/js/**/*.js', gulp.series('scripts'));
});