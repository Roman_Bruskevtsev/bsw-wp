<?php
/**
 * External product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/external.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="cart" action="<?php echo esc_url( $product_url ); ?>" method="get">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
    <div class="amount__block">
        <h6><?php echo AMOUNT; ?></h6>
        <div class="amount__field">
            <div class="minus amount"></div>
            <div class="number">
                <span class="numer">1</span>
                <span class="text"><?php echo COUNT; ?></span>
            </div>
            <div class="plus amount"></div>
        </div>
    </div>
	<button type="submit" class="single_add_to_cart_button to__cart red btn medium alt">
        <div class="layout"></div>
        <span class="text"><?php echo TOCART; ?></span>
        <span class="icon"></span>
    </button>

	<?php wc_query_string_form_fields( $product_url ); ?>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
</form>

<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
