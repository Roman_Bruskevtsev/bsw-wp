<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
$catIds = [];
$terms = get_the_terms( $post->ID, 'product_cat' );

foreach ($terms as $term) {
    array_push($catIds, $term->term_id);
}

$args = array(
    'posts_per_page'    => 4,
    'post_type'         => 'product',
    'post_status'       => 'publish',
    'orderby'           => 'rand',
    'post__not_in'      => array($post->ID),
    'tax_query'         => array(
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'id',
                'terms'    => $catIds
            )
        )
    )
);

if ( $related_products ) : 
    $query = new WP_Query( $args );

    if ( $query->have_posts() ) : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="related__product">
                <?php 
                $related_product = get_field('ralated_products', 'option');
                
                if( $related_product['title'] ) { ?>
                <div class="row">
                    <div class="col">
                        <h3 class="section__title text-center"><?php echo $related_product['title']; ?></h3>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <?php
                        $post_object = get_post( get_the_ID() );
                        setup_postdata( $GLOBALS['post'] =& $post_object );
                        wc_get_template_part( 'content', 'product' ); ?>
                <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; 
    wp_reset_postdata();
endif;

wp_reset_postdata();
