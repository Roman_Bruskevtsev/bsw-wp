<?php

defined( 'ABSPATH' ) || exit;

get_header();

get_template_part( 'template-parts/navigation/page-banner' );
get_template_part( 'template-parts/navigation/breadcrumbs' );

$category_id = (int) get_queried_object()->term_id;
$taxonomy = get_queried_object()->taxonomy;

if ( woocommerce_product_loop() ) { ?>
	<div class="products__wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="products__navigation">
                        <div class="sorting">
                        	<?php
                        	$page_link = get_term_link( $category_id, $taxonomy ); 
                        	$expensivest = $page_link.'?orderby=price-desc';
                        	$cheaper = $page_link.'?orderby=price';
                        	?>
                            <a href="<?php echo $cheaper; ?>" class="cheaper">
                                <i></i>
                                <span><?php echo CHEAPER; ?></span>
                            </a>
                            <a href="<?php echo $expensivest; ?>" class="expensivest">
                                <i></i>
                                <span><?php echo EXPENSIVEST; ?></span>
                            </a>
                        </div>
                        <?php
                        $args = array(
					       'hierarchical' 		=> true,
					       'hide_empty' 		=> false,
					       'parent' 			=> $category_id,
					       'taxonomy' 			=> $taxonomy
					    );
					    $subcats = get_terms($args);
					    
					    if( $subcats ) : ?>
                        <nav class="category__navigation">
                            <ul>
                            	<?php foreach ($subcats as $cat) { 
                            		$cat_id = (int) $cat->term_id;
                            		$term_link = get_term_link( $cat_id, $taxonomy );
                            		$active = '';
                            		if( $cat_id == $category_id ) $active = ' class="active"';
                            		?>
                            		<li>
	                                    <a href="<?php echo $term_link; ?>"<?php echo $active; ?>>
	                                    	<?php echo $cat->name; ?>		
	                                    </a>
	                                </li>
                            	<?php } ?>
                            </ul>
                        </nav>
                    	<?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
				<?php if ( wc_get_loop_prop( 'total' ) ) {
					while ( have_posts() ) {
						the_post();
						do_action( 'woocommerce_shop_loop' );

						wc_get_template_part( 'content', 'product' );
					}
				} ?>
			</div>
            <div class="row">
                <div class="col">
                    <?php do_action( 'woocommerce_after_shop_loop' ); ?>        
                </div>
            </div>
		</div>
	</div>
<?php } else {
	do_action( 'woocommerce_no_products_found' );
}

do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
