<?php

defined( 'ABSPATH' ) || exit;

global $product;

if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$thumbnail = get_the_post_thumbnail_url($post->ID, 'product-thumbnail');
$ribbon = '';
if( $product->get_sale_price() ){
	$ribbon = '<span class="ribbon special">'.SALE.'</span>';
} else if( get_field('is_product_new') ){
	$ribbon = '<span class="ribbon new">'.NEWPRODUCT.'</span>';
}
?>
<div <?php wc_product_class('col-sm-12 col-md-6 col-lg-4 col-xl-3'); ?>>
	<div class="product__block">
		<a href="<?php the_permalink();?>">
			<div class="thumbnail">
                <img src="<?php echo $thumbnail; ?>" alt="<?php the_title(); ?>">
            </div>
            <?php echo $ribbon; ?>
		</a>
		<div class="product__description">
           	<div class="title__row">
           		<h5><?php the_title(); ?></h5>
           		<div class="price__row">
           			<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
           		</div>
           	</div>
        </div>
        <div class="buy__row">
            <a href="<?php the_permalink();?>" class="to__cart btn medium">
                <div class="layout"></div>
                <span class="text"><?php echo TOCART; ?></span>
                <span class="icon"></span>
            </a>
        </div>
	</div>
</div>
