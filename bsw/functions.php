<?php
/**
 *
 * @package WordPress
 * @subpackage BSW
 * @since 1.0
 */

/*Trainers post type*/
require get_template_directory() . '/inc/post-type-function.php';

/*Translation*/
require get_template_directory() . '/inc/translation.php';
require get_template_directory() . '/inc/regions.php';

/*Theme setup*/
function bsw_setup() {
    load_theme_textdomain( 'bsw' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'woocommerce' );

    add_image_size( 'product-small', 160, 185, false );
    add_image_size( 'product-thumbnail', 350, 350, false );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'bsw' ),
        'category'      => __( 'Category Menu', 'bsw' )
    ) );
}
add_action( 'after_setup_theme', 'bsw_setup' );

/*App2drive styles and scripts*/
function bsw_scripts() {
    $version = '1.0.5';

    wp_enqueue_style( 'bsw-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'bsw-style', get_stylesheet_uri() );


    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'share-js', '//platform-api.sharethis.com/js/sharethis.js#property=5b77cc47f8352a0011896bd5&product=custom-share-buttons', array('jquery'), $version, true );
    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'select2-js', get_theme_file_uri( '/assets/js/select2.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'bsw_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {

    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Main Page Settings',
    //     'menu_title'    => 'Main Page',
    //     'parent_slug'   => $general['menu_slug']
    // ));
}


/*SVG support*/
function bsw_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'bsw_svg_types');

/*App2drive ajax*/
function bsw_ajaxurl() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        <?php if( get_field( 'api_key', 'option' ) ) { ?>
            var apiKey = '<?php the_field( 'api_key', 'option' ); ?>';
        <?php } ?>
    </script>
<?php
}
add_action('wp_footer','bsw_ajaxurl');

/*Change currency symbol*/
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
    $current_lang = ICL_LANGUAGE_CODE;

    if($current_lang == 'uk'){
        $symbol = ' грн.';
    } elseif ($current_lang == 'ru') {
        $symbol = ' грн.';
    } else {
        $symbol = ' UAH';
    }

    switch( $currency ) {
        case 'UAH': $currency_symbol = $symbol; break;
    }
    return $currency_symbol;
}

function bsw_no_limit_posts( $query ) {
    if( is_archive() ){
        $query->set( 'posts_per_page', 12 );
    }
}
add_action( 'pre_get_posts', 'bsw_no_limit_posts' );

/*Remove billing fields*/
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
 
function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_first_name']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_email']);
    unset($fields['billing']['billing_phone']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_company']);
    return $fields;
}
add_filter('woocommerce_checkout_fields', 'custom_woocommerce_billing_fields');

function custom_woocommerce_billing_fields($fields)
{
    $fields['billing']['billing_first_name'] = array(
        'label' => '',
        'placeholder' => YOURFIRSTNAME,
        'required' => true,
        'clear' => false,
        'type' => 'text',
        'class' => array('field__group')
    );

    $fields['billing']['billing_last_name'] = array(
        'label' => '',
        'placeholder' => YOURLASTNAME,
        'required' => true,
        'clear' => false,
        'type' => 'text',
        'class' => array('field__group')
    );

    $fields['billing']['billing_phone'] = array(
        'label' => '',
        'placeholder' => YOURPHONE,
        'required' => true,
        'clear' => false,
        'type' => 'tel',
        'class' => array('field__group')
    );

    $fields['billing']['billing_email'] = array(
        'label' => '',
        'placeholder' => YOUREMAIL,
        'required' => true,
        'clear' => false,
        'type' => 'email',
        'class' => array('field__group')
    );

    $fields['billing']['other_delivery'] = array(
        'label' => '',
        'placeholder' => FILLDELIVERY,
        'required' => false,
        'clear' => true,
        'type' => 'textarea',
        'class' => array('field__group')
    );

    return $fields;
}

add_action( 'woocommerce_checkout_update_order_meta', 'bsw_custom_checkout_field_update_order_meta' );

function bsw_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['other_delivery'] ) ) {
        update_post_meta( $order_id, 'other_delivery', sanitize_text_field( $_POST['other_delivery'] ) );
    }
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'bsw_checkout_field_display_admin_order_meta', 10, 1 );

function bsw_checkout_field_display_admin_order_meta($order){
    if( get_post_meta( $order->id, 'other_delivery', true ) ) echo '<p><strong>'.__('Other delivery type information').':</strong> ' . get_post_meta( $order->id, 'other_delivery', true ) . '</p>';
}
