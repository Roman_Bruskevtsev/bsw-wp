<?php if(function_exists('bcn_display')) { ?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="breacrumbs">
                <?php bcn_display(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>