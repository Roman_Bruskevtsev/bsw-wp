<?php 
$background = get_field('page_banner', 'option') ? ' style="background-image: url('.get_field('page_banner', 'option').')"' : '';
$title = '';
if( is_page() ){
    $title = get_the_title();
} elseif ( is_archive() ) {
    $term = get_queried_object();
    $title = $term->name;
} elseif( is_singular('product') ){
    $titles = get_the_terms(get_the_ID(), 'product_cat');
    $title = $titles[0]->name;
} ?>
<div class="container-fluid">
    <div class="row">
        <div class="page__banner text-center"<?php echo $background; ?>>
            <h1><?php echo $title; ?></h1>
        </div>
    </div>
</div>