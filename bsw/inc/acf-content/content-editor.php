<?php if( is_cart() || is_checkout() || is_account_page() ) { ?>
    <?php if( get_sub_field('content') ) { ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="cart__section">
                    <?php the_sub_field('content'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
<?php } else { ?>
    <?php if( get_sub_field('content') ) { ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="content">
                    <?php the_sub_field('content'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
<?php } ?>