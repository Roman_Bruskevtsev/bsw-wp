<?php if( get_sub_field('choose_categories') ){ 
$categories = get_sub_field('choose_categories'); ?>
<section class="categories__row">
    <div class="container">
        <div class="row justify-content-md-center">
            <?php foreach ($categories as $category) {
                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id ); 
            ?>
            <div class="col-lg-4">
                <a href="<?php echo get_category_link( $category->term_id ); ?>" class="category__banner">
                    <h5 class="title"><?php echo $category->name; ?></h5>
                    <span class="link"><?php echo TOCATEGORY; ?></span>
                    <?php if($image) { ?>
                    <div class="image">
                        <img src="<?php echo $image; ?>" alt="<?php echo $category->name; ?>">
                    </div>
                    <?php } ?>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>