<section class="products__section">
    <div class="container-fluid">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <h3 class="section__title text-center"><?php the_sub_field('title'); ?></h3>
            </div>
        </div>
        <?php }

        if( get_sub_field('choose_product_1') || 
            get_sub_field('choose_product_2') || 
            get_sub_field('choose_product_3') || 
            get_sub_field('choose_product_4') ) { 

            $product_ids = [];
            if( get_sub_field('choose_product_1')) array_push($product_ids, get_sub_field('choose_product_1'));
            if( get_sub_field('choose_product_2')) array_push($product_ids, get_sub_field('choose_product_2'));
            if( get_sub_field('choose_product_3')) array_push($product_ids, get_sub_field('choose_product_3'));
            if( get_sub_field('choose_product_4')) array_push($product_ids, get_sub_field('choose_product_4'));
            
            $args = array(
                'post__in' => $product_ids,
                'post_type'=> 'product'
            );
            $query = new WP_Query( $args );

            $i = 1;

            if ( $query->have_posts() ) { ?>
            <div class="row">
                <?php while ( $query->have_posts() ) { $query->the_post();
                    global $product;
                    $class = ($i == 4) ? ' product__hide' : '';
                    $thumbnail = get_the_post_thumbnail_url($post->ID, 'product-thumbnail');
                    $ribbon = '';
                    if( $product->get_sale_price() ){
                        $ribbon = '<span class="ribbon special">'.SALE.'</span>';
                    } else if( get_field('is_product_new') ){
                        $ribbon = '<span class="ribbon new">'.NEWPRODUCT.'</span>';
                    }
                    ?>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3<?php echo $class; ?>">
                    <div class="product__block" id="product-<?php echo get_the_ID(); ?>>">
                        <a href="<?php the_permalink();?>">
                            <div class="thumbnail">
                                <img src="<?php echo $thumbnail; ?>" alt="<?php the_title(); ?>">
                            </div>
                            <?php echo $ribbon; ?>
                        </a>
                        <div class="product__description">
                            <div class="title__row">
                                <h5><?php the_title(); ?></h5>
                                <div class="price__row">
                                    <?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="buy__row">
                            <a href="<?php the_permalink();?>" class="to__cart btn medium">
                                <div class="layout"></div>
                                <span class="text"><?php echo TOCART; ?></span>
                                <span class="icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <?php $i++; } ?>
            </div>
            <?php }
            wp_reset_postdata(); 
        } ?>
    </div>
</section>