<section class="partners__section">
    <div class="container-fluid">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <h3 class="section__title text-center"><?php the_sub_field('title'); ?></h3>
            </div>
        </div>
        <?php } ?>
        <?php if( have_rows('slide') ): ?>
        <div class="row">
            <div class="col">
                <div class="partner__slider">
                    <?php while ( have_rows('slide') ) : the_row(); ?>
                    <div class="slide">
                        <div class="banner__wrapper">
                            <a href="<?php the_sub_field('link'); ?>" target="_blank" class="banner">
                                <img src="<?php the_sub_field('logo'); ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if( get_sub_field('link') ) { ?>
        <div class="row">
            <div class="col">
                <div class="text-center">
                    <a class="btn simple__btn" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>