<?php if( have_rows('slide') ): ?>
<section class="slider__section">
    <div class="main__slider">
        <?php while ( have_rows('slide') ) : the_row(); ?>
        <div class="slide">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="slide__content">
                            <?php if( get_sub_field('product_image') ) { ?>
                            <div class="image__block">
                                <img src="<?php the_sub_field('product_image'); ?>" alt="<?php the_sub_field('title'); ?>">
                                <div class="circle"></div>
                                <?php if( get_sub_field('price') ) { ?>
                                <div class="price__circle"><?php the_sub_field('price'); ?></div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <div class="text__block">
                                <?php if( get_sub_field('title') ) { ?><h4><?php the_sub_field('title'); ?></h4><?php } ?>
                                <?php if( get_sub_field('subtitle') ) { ?><h3><?php the_sub_field('subtitle'); ?></h3><?php } ?>
                                <?php if( get_sub_field('red_link') || get_sub_field('grey_link') ) { ?>
                                <div class="link__row">
                                    <?php if( get_sub_field('red_link') ) { ?>
                                    <a href="<?php the_sub_field('red_link'); ?>" class="red__btn btn">
                                        <div class="layout"></div>
                                        <span class="text"><?php the_sub_field('red_link_label'); ?></span>
                                    </a>
                                    <?php } ?>
                                    <a href="<?php the_sub_field('grey_link'); ?>" class="simple__btn btn"><?php the_sub_field('grey_link_label'); ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
</section>
<?php endif; ?>