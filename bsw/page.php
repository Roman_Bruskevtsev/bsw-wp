<?php
/**
 *
 * @package WordPress
 * @subpackage BSW
 * @since 1.0
 * @version 1.0
 */
get_header();

get_template_part( 'template-parts/navigation/page-banner' );
get_template_part( 'template-parts/navigation/breadcrumbs' );

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'about_us_section' ): 
            get_template_part( 'inc/acf-content/about-us' );
        elseif( get_row_layout() == 'payment_delivery_row' ): 
            get_template_part( 'inc/acf-content/payment-delivery' );
        elseif( get_row_layout() == 'content_editor' ): 
            get_template_part( 'inc/acf-content/content-editor' );
        elseif( get_row_layout() == 'contact_row' ): 
            get_template_part( 'inc/acf-content/contact-row' );
        elseif( get_row_layout() == 'form_map' ): 
            get_template_part( 'inc/acf-content/contact-form-and-map' );
        elseif( get_row_layout() == 'icons_row' ): 
            get_template_part( 'inc/acf-content/icons-row' );
        elseif( get_row_layout() == 'partners_form' ): 
            get_template_part( 'inc/acf-content/partners-form' );
        elseif( get_row_layout() == 'partners_slider' ): 
            get_template_part( 'inc/acf-content/partners-slider' );
        endif;
    endwhile;
else :
    echo '
        <section class="padding__section">
            <div class="page__content">
                <div class="no__content">
                    <h1 class="text-center">'.__('Nothing to show', 'bsw').'</h1>
                </div>
            </div>
        </section>
    ';
endif;

get_footer();