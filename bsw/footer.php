<?php
/**
 *
 * @package WordPress
 * @subpackage BSW
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer>
        <?php if( get_field('subscribe_form_shortcode', 'option') ) { ?>
        <div class="suscribe__line">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="text"><?php the_field('subscribe_block_title', 'option'); ?></div>
                        <?php echo do_shortcode( get_field('subscribe_form_shortcode', 'option') ); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="contact__line">
            <div class="container">
                <div class="row justify-content-md-center">
                    <?php if( get_field('contact_phone', 'option') ) { ?>
                    <div class="col-md-4 col-lg-4 col-xl-3">
                        <div class="contact__block">
                            <div class="icon__block">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_icon.svg" alt="">
                            </div>
                            <div class="content__block">
                                <span class="title"><?php the_field('phone_block_title', 'option'); ?></span>
                                <a href="tel:<?php the_field('contact_phone', 'option'); ?>"><?php the_field('contact_phone', 'option'); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php } if( get_field('subscribe_form_shortcode', 'option') ) { ?>
                    <div class="col-md-4 col-lg-4 col-xl-3">
                        <div class="contact__block">
                            <div class="icon__block">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/email_icon.svg" alt="">
                            </div>
                            <div class="content__block">
                                <span class="title"><?php the_field('email_block_title', 'option'); ?></span>
                                <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-md-4 col-lg-4 col-xl-3">
                        <div class="contact__block">
                            <div class="content__block">
                                <span class="title"><?php the_field('social_block_title', 'option'); ?></span>
                                <ul class="social__row">
                                    <?php if( get_field('instagram', 'option') ) { ?>
                                    <li>
                                        <a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="instagram soc__icon">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.svg" alt="">
                                        </a>
                                    </li>
                                    <?php } if( get_field('facebook', 'option') ) { ?>
                                    <li>
                                        <a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="facebook soc__icon">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.svg" alt="">
                                        </a>
                                    </li>
                                    <?php } if( get_field('youtube', 'option') ) { ?>
                                    <li>
                                        <a href="<?php the_field('youtube', 'option'); ?>" target="_blank" class="youtube soc__icon">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube.svg" alt="">
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xl-3">
                        <div class="contact__block">
                            <div class="content__block">
                                <span class="title"><?php the_field('timetable_title', 'option'); ?></span>
                                <?php the_field('timetable', 'option'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>

</body>
</html>